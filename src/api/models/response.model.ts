export default class ResponseModel {
    public success: boolean;
    public message: string;
    public body: any;
}