import cors from "cors";
import express, { Application } from "express";

class Server {
  private app: Application;
  private port: string;

  constructor() {
    this.app = express();
    this.port = process.env.PORT || "3000";

    // Métodos iniciales
    this.middlewares();
    this.routes();
    this.monitor();
  }

  public middlewares(): void {
    // Habilito cors
    this.app.use(cors());

    // Lectura del body
    this.app.use(express.json());
  }

  public routes(): void {
    this.app.get("/", function (req: any, res: any) {
      res.send("Hello World");
    });
  }

  public monitor() {
    const ZongJi = require("zongji");

    const app = express();

    app.get("/", function (req: any, res: any) {
      res.send("Hello World");
    });

    const zongji = new ZongJi({
      host: "localhost",
      port: "3307",
      user: "root",
      password: "admin123",
    });

    zongji.on("binlog", function (evt: any) {
      evt.dump();
    });

    zongji.start({
      includeEvents: ["writerows"],
      // includeEvents: ['tablemap', 'writerows', 'updaterows', 'deleterows'],
      includeSchema: { slecsmart: true },
    });

    process.on("SIGINT", function () {
      console.log("Got SIGINT.");
      zongji.stop();
      process.exit();
    });
  }

  public listen(): void {
    this.app.listen(this.port, () => {
      console.log(`Server app listening at port ${this.port}`);
    });
  }
}

export default Server;
