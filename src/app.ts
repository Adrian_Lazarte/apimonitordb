import dotenv from "dotenv";
import Server from "./api/server";

// Configuro variables de entorno
dotenv.config();

// Start the Application Server
try {
  const server = new Server();
  server.listen();
} catch (err) {
  console.error("!Error starting server: ", err.message);
  throw err;
}
