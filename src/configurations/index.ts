import nconf from 'nconf';

// Read Configurations
console.log('ENVIRONMENT: ', process.env.NODE_ENV);
nconf.argv().env().file({ file: `./src/configurations/config.${process.env.NODE_ENV || 'dev'}.json` });

export interface IServerConfigurations {
  host: string;
  port: number;
  user: string;
  password: string;
  database: string;
}

export function getServerConfigs(): IServerConfigurations {
  return nconf.get("server");
}
